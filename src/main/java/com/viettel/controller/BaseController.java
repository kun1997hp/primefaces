package com.viettel.controller;


import com.google.gson.Gson;
import com.viettel.exception.AppException;
import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.model.BaseModel;
import com.viettel.model.Customer;
import com.viettel.persistence.CustomerDaoService;
import com.viettel.persistence.GenericDaoImplNewV2;
import com.viettel.util.Import;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseController<T extends  Serializable>  implements Serializable {
    private UploadedFile uploadedFile;
    public BaseModel obj;
    public LazyDataModel<T> model;
    public GenericDaoImplNewV2<T,Integer>  dao;
    private List<ColumnModel> columns;

    public BaseModel getObj() {
        return obj;
    }

    public void setObj(BaseModel obj) {
        this.obj = obj;
    }

    public LazyDataModel<T> getModel() {
        return model;
    }

    public void setModel(LazyDataModel model) {
        this.model = model;
    }

    public GenericDaoImplNewV2<T,Integer> getDao() {
        return dao;
    }

    public void setDao(GenericDaoImplNewV2 dao) {
        this.dao = dao;
    }

    public List<ColumnModel> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnModel> columns) {
        this.columns = columns;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public List<String> getAllTables(){
        List<?> listTable = dao.findListSQLAll("SELECT table_name FROM all_tables\n" +
                "WHERE owner = 'GIANG'");
        List<String> listString = new ArrayList<>();
        for (Object table: listTable) {
            listString.add(table.toString());
        }
        return listString;
    }

    public List<String> getFields() {
        String jsonInString = new Gson().toJson(this.obj);
        JSONObject mJSONObject = new JSONObject(jsonInString);
        List<String> fields = new ArrayList<>();
        for (String key : mJSONObject.keySet()) {
            fields.add(key);
        }
        return fields;
    }

    public void delete(T obj){
        try {
            dao.delete(obj);
            successMsg();
        } catch (AppException appExc){
            appExc.printStackTrace();
            errorMsg();
        }
    }

    public void saveOrUpdate(){
        try {
            dao.saveOrUpdate((T)this.obj);
            successMsg();
        } catch (AppException appExc){
            appExc.printStackTrace();
            errorMsg();
        }
    }

    public void upload() throws IOException, AppException {
        Path folder = Paths.get("D:\\MavenProject\\PrimefacesDemo\\src\\main\\uploads");
        String filename = FilenameUtils.getBaseName(uploadedFile.getFileName());
        String extension = FilenameUtils.getExtension(uploadedFile.getFileName());
        Path file = Files.createTempFile(folder, filename + "-", "." + extension);
        try (InputStream input = uploadedFile.getInputstream()) {
            Files.copy(input, file, StandardCopyOption.REPLACE_EXISTING);
        }
        FacesMessage message = new FacesMessage("Successful", file.getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
        Import<Serializable> importer = new Import<Serializable>() {
            @Override
            protected Map<Integer, String> getIndexMapFieldClass() {
                Map<Integer, String> integerStringMap = new HashMap<>();
                int i = 0;
                integerStringMap.put(i++, "NAME");
                integerStringMap.put(i++, "EMAIL");
                return integerStringMap;
            }
            @Override
            protected String getDateFormat() {
                return null;
            }
        };
        String pathFile = folder.toString()+"\\"+file.getFileName();
        System.out.println(pathFile);
        System.out.println(pathFile);
        InputStream input = new FileInputStream(pathFile);
        List<Serializable> datas = importer.getDatas(input, 0, "2-");
        for(Serializable data: datas){
            System.out.println(data);
        }
        System.out.println(datas);
    }

    public void errorMsg() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Có lỗi xảy ra!"));
    }

    public void successMsg() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Thực hiện thành công!"));
    }

    public void createDynamicColumns() {
        String[] columnKeys = this.getFields().stream().toArray(String[]::new);
        columns = new ArrayList<ColumnModel>();

        for(String columnKey : columnKeys) {
            String key = columnKey.trim();
            columns.add(new ColumnModel(columnKey.toUpperCase(), columnKey));
        }
    }

    public void preEdit(BaseModel obj){
        if(obj == null) {this.obj = this.obj.build();}
        else {this.obj = obj;}
    }

    static public class ColumnModel implements Serializable {

        private String header;
        private String property;

        public ColumnModel(String header, String property) {
            this.header = header;
            this.property = property;
        }

        public String getHeader() {
            return header;
        }

        public String getProperty() {
            return property;
        }
    }
}
