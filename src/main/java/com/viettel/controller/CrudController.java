package com.viettel.controller;

import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.model.Customer;
import com.viettel.model.Product;
import com.viettel.model.Type;
import com.viettel.persistence.CustomerDaoService;
import com.viettel.persistence.ProductDaoService;
import com.viettel.persistence.TypeDaoService;
import com.viettel.util.Import;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ManagedBean(name = "crud")
@ViewScoped

public class CrudController  extends  BaseController {
    private boolean showTable;
    private UploadedFile uploadedFile;

    public boolean isShowTable() {
        return showTable;
    }

    public void setShowTable(boolean showTable) {
        this.showTable = showTable;
    }

    @PostConstruct
    public void onStart() {
        Customer obj = new Customer();
        CustomerDaoService dao = new CustomerDaoService();
        this.setDao(dao);
        this.setObj(obj);
        this.setShowTable(false);
    }

    public void onTableChange(SelectEvent event) throws ClassNotFoundException {
        for (Object tableName : this.getFields()) {
            if (event.getObject().toString().equals("CUSTOMER")) {
                //goi onStart
                Customer obj = new Customer();
                CustomerDaoService dao = new CustomerDaoService();
                LazyDataModel<Customer> model = new LazyDataModelBaseNew<Customer, Integer>(dao);
                this.setObj(obj);
                this.setDao(dao);
                this.setModel(model);
                this.setShowTable(true);
                createDynamicColumns();
            }else  if (event.getObject().toString().equals("PRODUCT")) {
                Product obj = new Product();
                ProductDaoService dao = new ProductDaoService();
                LazyDataModel<Product> model = new LazyDataModelBaseNew<Product, Integer>(dao);
                this.setObj(obj);
                this.setDao(dao);
                this.setModel(model);
                this.setShowTable(true);
                createDynamicColumns();
            }
            else if(event.getObject().toString().equals("TYPE")) {
                Type obj = new Type();
                TypeDaoService dao = new TypeDaoService();
                LazyDataModel<Type> model = new LazyDataModelBaseNew<Type, Integer>(dao);
                this.setObj(obj);
                this.setDao(dao);
                this.setModel(model);
                this.setShowTable(true);
                createDynamicColumns();
            }
            else {
                System.out.println("#### vô đây");
                this.setShowTable(false);
                createDynamicColumns();
            }
        }
    }
}