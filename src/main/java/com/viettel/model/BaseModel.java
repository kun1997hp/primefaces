package com.viettel.model;

public interface BaseModel {
    public BaseModel build();
}
