package com.viettel.persistence.common;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.IlikeExpression;
import org.hibernate.criterion.MatchMode;

public class EscapedILikeExpression extends IlikeExpression {
    public static final String ESCAPE_CHAR = " ESCAPE '`' ";

    /**
     * @param propertyName
     * @param value
     */
    public EscapedILikeExpression(String propertyName, Object value) {
        super(propertyName, value);
// TODO Auto-generated constructor stub
    }

    /**
     * @param propertyName
     * @param value
     * @param matchMode
     */
    public EscapedILikeExpression(String propertyName, String value, MatchMode matchMode) {
        super(propertyName, value, matchMode);
// TODO Auto-generated constructor stub
    }

    public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery)
            throws HibernateException {
        String sql = super.toSqlString(criteria, criteriaQuery);
        sql = sql + ESCAPE_CHAR;
        return sql;
    }
}