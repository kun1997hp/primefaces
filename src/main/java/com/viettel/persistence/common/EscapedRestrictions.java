package com.viettel.persistence.common;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;

public class EscapedRestrictions {
    static String HIBERNATE_ESCAPE_CHAR = "`";
    public static Criterion ilike(String propertyName, Object value) {
        String value1 = value.toString();
        value1 = value1.replaceAll("`",HIBERNATE_ESCAPE_CHAR + "`");
        value1 = value1.replaceAll("_",HIBERNATE_ESCAPE_CHAR + "_");
        return new EscapedILikeExpression(propertyName, value1);
    }
    public static Criterion ilike(String propertyName, String value) {
        value = value.replaceAll("`",HIBERNATE_ESCAPE_CHAR + "`");
        value = value.replaceAll("_",HIBERNATE_ESCAPE_CHAR + "_");
        return new EscapedILikeExpression(propertyName, value);
    }

    public static Criterion ilike(String propertyName, String value, MatchMode matchMode) {
        value = value.replaceAll("`",HIBERNATE_ESCAPE_CHAR + "`");
        value = value.replaceAll("_",HIBERNATE_ESCAPE_CHAR + "_");
        return new EscapedILikeExpression(propertyName, value, matchMode);
    }
}