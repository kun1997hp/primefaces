package com.viettel.util;

import com.google.gson.GsonBuilder;
import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.primefaces.event.FileUploadEvent;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

public abstract class Import<T extends Serializable> {
    protected static final Logger logger = LogManager.getLogger(Import.class);
    private Class<T> domainClass ;
    private Map<Integer,String> indexMapFieldClass = getIndexMapFieldClass();
    private String dateFormat = "MM/dd/yyyy";
    //20190524_tudn_start tac dong toan trinh SR GNOC
    private Integer rowHeaderNumber;
    private boolean isReplace = true;
    private boolean isReplaceSpace = true;
    //20190524_tudn_end tac dong toan trinh SR GNOC
    //20190307_tudn_start validate dai mang IP
    private boolean isCheckDate = true;
    //20190307_tudn_start validate dai mang IP
    /**
     *
     * @return Map<Integer,String>
     * <br>Start column = 0
     * @author huynx6
     *
     */
    protected abstract Map<Integer,String> getIndexMapFieldClass();
    protected abstract String getDateFormat();
    private Map<Integer,String> mapHeader = getMapHeader();
    private boolean isParamNotIndex= false;
    @SuppressWarnings("unchecked")
    public Import() {
        super();
        java.lang.reflect.Type genericSuperclass = getClass().getGenericSuperclass();
        if(genericSuperclass!=null && genericSuperclass instanceof ParameterizedType){
            this.domainClass = (Class<T>) ((ParameterizedType) genericSuperclass).getActualTypeArguments()[0];
            if("java.io.Serializable".equalsIgnoreCase(domainClass.getCanonicalName())) {
                this.domainClass = (Class<T>) BasicDynaClass.class;
            }
        }else{
            this.domainClass = (Class<T>) BasicDynaClass.class;
        }
        if (getDateFormat()!=null)
            dateFormat = getDateFormat();
    }

    public List<T> getDatas(FileUploadEvent event, Integer sheetNumber, String sline){
        try {
            return getDatas(event.getFile().getInputstream(), sheetNumber, sline, null);
        } catch (IOException e) {
            logger.error(e.getMessage(),e);
        }
        return null;
    }
    public List<T> getDatas(FileUploadEvent event, String sheetName, String sline){
        try {
            return getDatas(event.getFile().getInputstream(), null, sline, sheetName);
        } catch (IOException e) {
            logger.error(e.getMessage(),e);
        }
        return null;
    }
    public List<T> getDatas(InputStream inputStream, Integer sheetNumber,String sline){
        return getDatas(inputStream, sheetNumber, sline, null);
    }
    public List<T> getDatas(InputStream inputStream,String sline, String sheetName){
        return getDatas(inputStream, null, sline, sheetName);
    }

    private List<T> getDatas(InputStream inputStream, Integer sheetNumber,String sline, String sheetName){

        List<T> datas = new ArrayList<>();
        try(Workbook workbook = WorkbookFactory.create(inputStream)) {
            datas = getDatas(workbook, sheetNumber, sline, sheetName);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
        return datas;
    }
    public List<T> getDatas(Workbook workbook , Integer sheetNumber,String sline){
        return getDatas(workbook, sheetNumber, sline, null);
    }
    public List<T> getDatas(Workbook workbook , String sheetName,String sline){
        return getDatas(workbook, null, sline, sheetName);
    }
    @SuppressWarnings("unchecked")
    private List<T> getDatas(Workbook workbook , Integer sheetNumber,String sline, String sheetName){
        try {
            if (workbook==null)
                throw new NullPointerException();

            String[] mulLine = sline.split(",",-1);

            List<T> list = new LinkedList<T>();
            Sheet sheet = null;
            if (sheetNumber!=null)
                sheet = workbook.getSheetAt(sheetNumber);
            else if (sheetName!=null)
                sheet = workbook.getSheet(sheetName);
            if(sheet==null)
                return list;
            List<Field> fields = new ArrayList<>();
            List<String> fieldNames = new ArrayList<>();
            try {
                Class<?> cls = domainClass;
                fields = Arrays.asList(cls.getDeclaredFields());
                for (Field field : fields) {
                    fieldNames.add(cls.getName()+"."+field.getName());
                }
            } catch (SecurityException e) {
                logger.error(e.getMessage(),e);
            }
            int line=1;
            String sLine;
            boolean isFindHeader =false;
            for (Iterator<Row> rowIterator = sheet.iterator(); rowIterator
                    .hasNext();) {
                Row row = (Row) rowIterator.next();
                boolean cont = false;
                sLine="";
                try {
                    if (Pattern.compile("\""+line+"\"").matcher(new GsonBuilder().create().toJson(mulLine)).find())
                        cont = true;
                    else {
                        for (String _sline : mulLine) {
                            String[] strip = _sline.split("-",-1);
                            if (strip.length ==1) {
                                if(line == Integer.parseInt(strip[0])){
                                    cont=true;
                                    break;
                                }
                            }else if (strip.length ==2) {
                                if(strip[1].length()==0){
                                    if(line >= Integer.parseInt(strip[0])){
                                        cont=true;
                                        break;
                                    }
                                }else if(line >= Integer.parseInt(strip[0]) && line <= Integer.parseInt(strip[1]) ){
                                    cont=true;
                                    break;
                                }

                            }

                        }
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(),e);
                }
                line++;
                if(!cont)
                    continue;
                if(mapHeader!=null){

                    if(!isFindHeader){
                        boolean isCont =false;
                        for(int i=row.getFirstCellNum();i<row.getLastCellNum();i++){
                            if(!isFindHeader && row.getCell(i).getCellType()==Cell.CELL_TYPE_STRING && mapHeader.get(1).equalsIgnoreCase(row.getCell(i).getStringCellValue())){
                                isFindHeader=true;
                                isCont = true;
                                if(i>0){
                                    Map<Integer, String>_indexMapFieldClass = new HashMap<Integer, String>();
                                    for (Iterator<Integer> iterator = indexMapFieldClass.keySet().iterator(); iterator.hasNext();) {
                                        Integer col = (Integer) iterator.next();
                                        _indexMapFieldClass.put(i+col, indexMapFieldClass.get(col));
                                    }
                                    indexMapFieldClass.clear();
                                    indexMapFieldClass.putAll(_indexMapFieldClass);
                                }
                                if(!isParamNotIndex)
                                    break;
                            }
                            if(i>1 && isParamNotIndex && isFindHeader)
                                indexMapFieldClass.put(i,row.getCell(i).getStringCellValue());
                        }
                        if(isCont)
                            continue;
                    }
                    if(!isFindHeader)
                        continue;


                }
                {//Read data
                    Class<?> obj = domainClass;

                    Object objInstance = obj.newInstance();
                    if (objInstance instanceof BasicDynaClass) {
                        List<DynaProperty> dynaProperties = new ArrayList<DynaProperty>();
                        for (Integer idx : indexMapFieldClass.keySet()) {
                            dynaProperties.add(new DynaProperty(normalizeParamCode(indexMapFieldClass.get(idx)), String.class));
                        }
                        objInstance = new BasicDynaClass(sheetName, null, dynaProperties.toArray(new DynaProperty[0]) );
                        objInstance = ((BasicDynaClass) objInstance).newInstance();
                    }
                    for (Iterator<Integer> columnIndex = indexMapFieldClass.keySet().iterator(); columnIndex
                            .hasNext();) {
                        Integer nColumn = columnIndex.next();
                        Cell cell ;

                        if( row.getCell(nColumn) == null ){
                            cell = row.createCell(nColumn);
                        } else {
                            cell = row.getCell(nColumn);
                        //
                        }
                        Object cellValue ;
                        Date cellDateValue = null;
                        switch (cell.getCellType()) {
                            case Cell.CELL_TYPE_BLANK:
                                cellValue = new String();
                                break;
                            case Cell.CELL_TYPE_BOOLEAN:
                                cellValue = cell.getBooleanCellValue();
                                break;
                            case Cell.CELL_TYPE_FORMULA:
                                try {
                                    cellValue = cell.getRichStringCellValue().getString();
                                } catch (Exception e) {
                                    logger.debug(e.getMessage(), e);
                                    cellValue = String.valueOf(cell.getNumericCellValue()).replaceAll("\\.0+$", "");
                                }
                                break;
                            case Cell.CELL_TYPE_NUMERIC:
                                //20190307_tudn_start validate dai mang IP
                                if(isCheckDate) {
                                    //20190307_tudn_end validate dai mang IP
                                    try {
                                        cellDateValue = cell.getDateCellValue();
                                    } catch (Exception e) {
                                        logger.debug(e.getMessage(), e);
                                    }
                                    cell.setCellType(Cell.CELL_TYPE_STRING);
                                    cellValue = cell.getStringCellValue();
                                    //20190307_tudn_start validate dai mang IP
                                }else{
                                    if(DateUtil.isCellDateFormatted(cell))
                                    {
                                        cell.setCellType(Cell.CELL_TYPE_STRING);
                                        cellValue = "RowDataIsDate";
                                    }else{
                                        cell.setCellType(Cell.CELL_TYPE_STRING);
                                        cellValue = cell.getStringCellValue();
                                    }
                                }
                                //20190307_tudn_end validate dai mang IP

                                break;
                            case Cell.CELL_TYPE_ERROR:
                                cellValue="";
                                break;
                            case Cell.CELL_TYPE_STRING:
                            default:
                                cellValue = cell.getStringCellValue();
                                break;
                        }
                        sLine +=cellValue.toString();
                        if(fieldNames.contains(domainClass.getName()+"."+indexMapFieldClass.get(nColumn))){
                            try {
                                //Field field = fields.get(index);
                                for (Field field : fields) {
                                    if(field.getName().equalsIgnoreCase(indexMapFieldClass.get(nColumn))){
                                        if (field .getType().getName().contains("java.lang.Long")) {
                                            PropertyUtils.setSimpleProperty(objInstance, field.getName(), Long.parseLong(cellValue.toString()));
                                        } else if (field.getType().getName().contains("java.lang.Integer"))  {
                                            PropertyUtils.setSimpleProperty(objInstance, field.getName(), Integer.parseInt(cellValue.toString()));
                                        } else if (field.getType().getName().contains("java.lang.String"))  {
                                            PropertyUtils.setSimpleProperty(objInstance, field.getName(), cellValue);
                                        } else if (field.getType().getName().contains("Date"))  {
                                            if (cellDateValue!=null)
                                                PropertyUtils.setSimpleProperty(objInstance, field.getName(),cellDateValue);
                                            else{
                                                try {
                                                    DateFormat formatter = new SimpleDateFormat(dateFormat);
                                                    PropertyUtils.setSimpleProperty(objInstance, field.getName(),formatter.parse(cellValue.toString()) );
                                                } catch (Exception e) {
                                                    logger.debug(e.getMessage(),e);
                                                }
                                            }
                                        } else if (field.getType().getName().contains("java.lang.Object"))  {
                                            PropertyUtils.setSimpleProperty(objInstance, field.getName(), cellValue);
                                        }
                                        break;
                                    }
                                }

                            } catch (IllegalArgumentException e) {
                                logger.debug(e.getMessage(),e);

                            }
                        }else {
                            if (objInstance instanceof BasicDynaBean) {
                                PropertyUtils.setSimpleProperty(objInstance, normalizeParamCode(indexMapFieldClass.get(nColumn)), cellValue);
                            }
                        }
                    }
                    if(sLine.trim().length()==0)
                        break;
                    list.add((T) objInstance);
                }
            }
            return list;
        } catch (Exception e) {
            logger.debug(e.getMessage(),e);
            System.err.println(e.getMessage());
            return null;
        }

    }

    //20190524_tudn_start tac dong toan trinh SR GNOC
    public List<T> getDatasWS(Workbook workbook, Integer sheetNumber, String sline, String sheetName) {
        try {
            if (workbook == null) {
                throw new NullPointerException();
            }

            String[] mulLine = sline.split(",", -1);

            List<T> list = new LinkedList<T>();
            Sheet sheet = null;
//			System.out.println("sheetName: " + sheetName);
            if (sheetNumber != null) {
                sheet = workbook.getSheetAt(sheetNumber);
            } else if (sheetName != null) {
                sheet = workbook.getSheet(sheetName);
            }
            List<Field> fields = new ArrayList<>();
            try {
                Class<?> cls = domainClass;
                fields = Arrays.asList(cls.getDeclaredFields());
            } catch (SecurityException ex) {
                logger.error(ex.getMessage(), ex);
            }
            int line = 1;
            String sLine;
            boolean isFindHeader = false;
            if (sheet != null) {
                for (Iterator<Row> rowIterator = sheet.iterator(); rowIterator
                        .hasNext(); ) {
                    Row row = (Row) rowIterator.next();
                    boolean cont = false;
                    sLine = "";
                    if (rowHeaderNumber != null) {
                        if (line == rowHeaderNumber) {
                            if (indexMapFieldClass != null) {
                                indexMapFieldClass.clear();
                            } else {
                                indexMapFieldClass = new HashMap<>();
                            }
                            for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
                                if(row.getCell(i) != null) {
                                    if (isReplace) {
                                        //20170817_hienhv4_fix import param to mop_start
                                        if (isReplaceSpace) {
                                            indexMapFieldClass.put(i, row.getCell(i).getStringCellValue().trim()
                                                    .replace(" ", "_")
                                                    .replace(".", "_")
                                                    .replace("(", "").replace(")", "")
                                                    .replace("[", "").replace("]", "")
                                                    .toLowerCase());
                                        } else {
                                            indexMapFieldClass.put(i, row.getCell(i).getStringCellValue().trim()
                                                    .replace(".", "_")
                                                    .replace("(", "").replace(")", "")
                                                    .replace("[", "").replace("]", "")
                                                    .toLowerCase());
                                        }
                                        //20170817_hienhv4_fix import param to mop_end
                                    } else {
                                        //20170817_hienhv4_fix import param to mop_start
                                        if (isReplaceSpace) {
                                            indexMapFieldClass.put(i, row.getCell(i).getStringCellValue().trim()
                                                    .replace(" ", "__")
                                                    .replace(".", "___")
                                                    .replace("(", "").replace(")", "")
                                                    .replace("[", "").replace("]", "")
                                                    .toLowerCase());
                                        } else {
                                            indexMapFieldClass.put(i, row.getCell(i).getStringCellValue().trim()
                                                    .replace("(", "").replace(")", "")
                                                    .replace("[", "").replace("]", "")
                                                    .replace(".", "___").toLowerCase());
                                        }
                                        //20170817_hienhv4_fix import param to mop_emd
                                    }
                                }
                            }
//							System.out.println("indexMapFieldClass: " + indexMapFieldClass);
                        }
                    }

                    try {
                        if (Pattern.compile("\"" + line + "\"").matcher(new GsonBuilder().create().toJson(mulLine)).find()) {
                            cont = true;
                        } else {
                            for (String _sline : mulLine) {
                                String[] strip = _sline.split("-", -1);
                                if (strip.length == 1) {
                                    if (line == Integer.parseInt(strip[0])) {
                                        cont = true;
                                        break;
                                    }
                                } else if (strip.length == 2) {
                                    if (strip[1].length() == 0) {
                                        if (line >= Integer.parseInt(strip[0])) {
                                            cont = true;
                                            break;
                                        }
                                    } else if (line >= Integer.parseInt(strip[0]) && line <= Integer.parseInt(strip[1])) {
                                        cont = true;
                                        break;
                                    }

                                }

                            }
                        }
                    } catch (Exception e1) {
                        logger.error(e1.getMessage(), e1);
                    }
                    line++;
                    if (!cont) {
                        continue;
                    }
                    if (mapHeader != null) {

                        if (!isFindHeader) {
                            boolean isCont = false;
                            for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
                                if (row.getCell(i) != null && row.getCell(i).getCellType() == Cell.CELL_TYPE_STRING && mapHeader.get(1).equalsIgnoreCase(row.getCell(i).getStringCellValue())) {
                                    isFindHeader = true;
                                    isCont = true;
                                    if (i > 0) {
                                        Map<Integer, String> _indexMapFieldClass = new HashMap<Integer, String>();
                                        for (Iterator<Integer> iterator = indexMapFieldClass.keySet().iterator(); iterator.hasNext(); ) {
                                            Integer col = (Integer) iterator.next();
                                            _indexMapFieldClass.put(i + col, indexMapFieldClass.get(col));
                                        }
                                        indexMapFieldClass.clear();
                                        indexMapFieldClass.putAll(_indexMapFieldClass);
                                    }
                                    break;
                                }
                            }
                            if (isCont) {
                                continue;
                            }
                        }
                        if (!isFindHeader) {
                            continue;
                        }

                    }
                    {//Read data
                        Class<?> obj = domainClass;

                        Object objInstance = obj.newInstance();
                        if (objInstance instanceof BasicDynaClass) {
                            List<DynaProperty> dynaProperties = new ArrayList<DynaProperty>();
                            for (Integer idx : indexMapFieldClass.keySet()) {
                                dynaProperties.add(new DynaProperty(indexMapFieldClass.get(idx), String.class));
                            }
                            objInstance = new BasicDynaClass(sheetName, null, dynaProperties.toArray(new DynaProperty[dynaProperties.size()]));
                            objInstance = ((BasicDynaClass) objInstance).newInstance();
                        }
                        for (Iterator<Integer> columnIndex = indexMapFieldClass.keySet().iterator(); columnIndex
                                .hasNext(); ) {
                            Integer nColumn = columnIndex.next();
                            Cell cell;
                            if (row.getCell(nColumn) == null) {
                                cell = row.createCell(nColumn);
                            } else {
                                cell = row.getCell(nColumn);
                            }
                            Object cellValue;
                            switch (cell.getCellType()) {
                                case Cell.CELL_TYPE_BLANK:
                                    cellValue = new String();
                                    break;
                                case Cell.CELL_TYPE_BOOLEAN:
                                    cell.setCellType(Cell.CELL_TYPE_STRING);
                                    cellValue = cell.getStringCellValue();
                                    break;
                                case Cell.CELL_TYPE_FORMULA:
                                    try {
                                        cellValue = cell.getRichStringCellValue().getString();
                                    } catch (Exception e) {
                                        logger.debug(e.getMessage(), e);
                                        cellValue = String.valueOf(cell.getNumericCellValue()).replaceAll("\\.0+$", "");
                                    }
                                    break;
                                case Cell.CELL_TYPE_NUMERIC:
                                    cell.setCellType(Cell.CELL_TYPE_STRING);
                                    cellValue = cell.getStringCellValue();
                                    break;
                                case Cell.CELL_TYPE_STRING:
                                default:
                                    cellValue = cell.getStringCellValue();
                                    break;
                            }
                            sLine += cellValue.toString();

                            if (fields.toString().contains(domainClass.getName() + "." + indexMapFieldClass.get(nColumn) + ",") || fields.toString().contains(domainClass.getName() + "." + indexMapFieldClass.get(nColumn) + "]")) {
//								logger.info("---1:" + fields.toString());
//								logger.info("---2:" + domainClass.getName() + "." + indexMapFieldClass.get(nColumn));
//								logger.info("---3:" + cellValue.toString());
                                try {
                                    //Field field = fields.get(index);
                                    for (Field field : fields) {
                                        if (field.getName().equalsIgnoreCase(indexMapFieldClass.get(nColumn))) {
                                            if (field.getType().getName().contains("java.lang.Long")) {
                                                PropertyUtils.setSimpleProperty(objInstance, field.getName(), Long.parseLong(cellValue.toString()));
                                            } else if (field.getType().getName().contains("java.lang.Integer")) {
                                                PropertyUtils.setSimpleProperty(objInstance, field.getName(), Integer.parseInt(cellValue.toString()));
                                            } else if (field.getType().getName().contains("java.lang.String")) {
                                                PropertyUtils.setSimpleProperty(objInstance, field.getName(), cellValue);
                                            } else if (field.getType().getName().contains("Date")) {
                                                DateFormat formatter = new SimpleDateFormat(dateFormat);
                                                PropertyUtils.setSimpleProperty(objInstance, field.getName(), formatter.parse(cellValue.toString()));
                                            } else if (field.getType().getName().contains("java.lang.Object")) {
                                                PropertyUtils.setSimpleProperty(objInstance, field.getName(), cellValue);
                                            }
                                            break;
                                        }
                                    }

                                } catch (IllegalArgumentException ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            } else {
                                if (objInstance instanceof BasicDynaBean) {
                                    PropertyUtils.setSimpleProperty(objInstance, normalizeParamCode(indexMapFieldClass.get(nColumn)), cellValue);
                                }
                            }
                        }
                        if (sLine.trim().length() == 0) {
                            break;
                        }
                        list.add((T) objInstance);
                    }
                }
            }
            return list;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }

    }

    public static String normalizeParamCode(String paramCode) {
        if (paramCode != null) {
            return paramCode
                    .replace("(", "").replace(")", "")
                    .replace("[", "").replace("]", "");
        }
        return null;
    }

    public Integer getRowHeaderNumber() {
        return rowHeaderNumber;
    }

    public void setRowHeaderNumber(Integer rowHeaderNumber) {
        this.rowHeaderNumber = rowHeaderNumber;
    }

    public boolean isIsReplace() {
        return isReplace;
    }

    public void setIsReplace(boolean isReplace) {
        this.isReplace = isReplace;
    }

    public boolean isIsReplaceSpace() {
        return isReplaceSpace;
    }

    public void setIsReplaceSpace(boolean isReplaceSpace) {
        this.isReplaceSpace = isReplaceSpace;
    }
    //20190524_tudn_end tac dong toan trinh SR GNOC

    //20190307_tudn_start validate dai mang IP
    public boolean isCheckDate() {
        return isCheckDate;
    }

    public void setCheckDate(boolean checkDate) {
        isCheckDate = checkDate;
    }
    //20190307_tudn_end validate dai mang IP

    public void setIndexMapFieldClass(Map<Integer,String> indexMapFieldClass) {
        this.indexMapFieldClass = indexMapFieldClass;
    }
    public Map<Integer,String> getMapHeader() {
        return mapHeader;
    }
    public void setMapHeader(Map<Integer,String> mapHeader) {
        this.mapHeader = mapHeader;
    }
    public void setDomainClass(Class<T> domainClass) {
        this.domainClass = domainClass;
    }

    public void setParamNotIndex(boolean paramNotIndex) {
        isParamNotIndex = paramNotIndex;
    }
}
